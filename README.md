﻿# Semillero UIS 2019-1

<img src="Semillero_2019-I/imgs/Banner_Logo.jpeg" style="width:400px;">

# Opción 1: Notebooks de forma nativa.

## Descargar Anaconda
En esta opción utilizaremos Anaconda cloud, el cual puede descargarse [aquí](https://www.anaconda.com/distribution/)
Seleccionamos el sistema operativo de nuestra máquina, la arquitectura, descargamos e instalamos la aplicación.

## Anaconda prompt
Anaconda prompt es un terminal de anaconda que nos permite instalar aquellas dependencias que necesitemos.

- Primero, ejecutamos anaconda prompt en nuestra máquina.
- Después, navegamos hasta la carpeta donde se encuentra nuestro archivo de dependencias. el Path del archivo es: 
 Ruta/del/repositorio/en/tu/maquina/semillero-2019-1/
- Utilizando Anaconda prompt, nos moveremos al directorio de las dependencias, para ello, introducimos el siguiente comando:
 cd Ruta/del/repositorio/en/tu/maquina/semillero-2019-1/Anaconda_libraries/
- Luego introduciremos el siguiente comando para instalar las dependencias basandonos en un archivo:
 conda env create -f semilleroUIS2019.yml
- Seguimos las instrucciones en pantalla hasta que termine de instalar todas las librerías.

Los pasos anteriormente descritos se realizan sólo la primer vez que ejecutamos anaconda en nuestra computadora.

## Anaconda navigator
Anaconda navigator es una aplicación de anaconda que nos permite lanzar otras aplicaciones. En nuestro semillero, la utilizaremos principalmente para lanzar Jupyter Notebooks, para ello debemos seguir los siguientes pasos:

- Primero, ejecutamos Anaconda navigator en nuestra máquina.
- Después, debemos seleccionar nuestro "Environment", para realizar esto, en la parte superior de la aplicación donde dice "Applications on:" cambiamos "Root" por "semilleroUIS2019".
- Luego, hacemos click en Jupyter Noteboos en el botón launch, lo cual abrirá un Jupyter Notebook donde podremos trabajar.
- Creamos un notebook desde cero o ejecutamos un notebook existente del curso (En función de tus necesidades).

## Repositorio

El repositorio del curso se encuentra en: https://gitlab.com/bivl2ab/pvd-courses-2019/semillero-2019-1/semillero-2019-1.git

Desde allí podremos descargar el repositorio a nuestra máquina y ejecutarlo de forma local utilizando los pasos descritos en la sección de Anaconda Navigator.

# Opción 2: Notebooks a través de una máquina virtual


## Instrucciones de configuración
Para poder ejecutar experimentos desde casa es necesario instalar VirtualBox, su pack de extensiones y utilidades. Finalmente montar la máquina virtual.

- VirtualBox puede descargarse desde su sitio oficial [aquí](https://www.virtualbox.org/wiki/Downloads)  (Seleccionar el sistema operativo que esté instalado en tu máquina local).

- Virtualbox extension pack puede descargarse desde su sitio oficial [aquí](https://download.virtualbox.org/virtualbox/6.0.8/Oracle_VM_VirtualBox_Extension_Pack-6.0.8.vbox-extpack), para instalarlo se debe tener previamente instalado VirtualBox.

- La máquina virtual puede descargarse [aquí](https://drive.google.com/file/d/1KxCUZlXDgyvJzfs6s7EfegMVS1HL_bXq/view?usp=sharing)



# Máquina Virtual

## Montando la máquina virtual:

- Abrimos VirtualBox

- Seleccionamos Archivo/Importar servicio virtualizado

- Damos click sobre la carpeta y buscaremos el directorio donde descargamos nuestra máquina virtual (datasci-2018b.ova)

Usaremos esta máquina virtual que tiene instalado un entorno Python Anaconda con Jupyter Notebooks disponibles en  [localhost:8008/tree](http://localhost:8008/tree) una vez que la máquina arranca.

**Observa la configuración de la máquina**

- Si tu máquina física tiene al menos 4GB de memoria configura la máquina virtual **con 2GB de memoria** (se recomienda utilizar máximo el 50% de la memoria disponible)
- Aunque casi no necesitarás un terminal, el interfaz de Jupyter Notebooks tiene un terminal para acceder a través del navegador. En cualquier caso, la máquina virtual tiene un servidor SSH en el puerto 2222 con user/user como usuario y pwd. Si tu máquina física es mac o linux usa `ssh -p 2222 user@localhost` para conectarte. Si es windows, usa [putty](https://www.putty.org/)
- Si compartes una carpeta entre la física y virtual asegúrate que **el nombre cone el que se comparte** sea `share` (aunque el nombre de la carpeta en la máquina física puede ser distinto)

recuerda que debes tener carpetas compartidas, para ello verifica que existan registros en: 
    - Click derecho sobre la máquina virtual
    - Configuración
    - Carpetas Compartidas
    - En caso de que no existan registros, agregar una carpeta compartida.

**Para montar la carpeta compartida** ejecuta lo siguiente en un terminal y la carpeta aparecerá en /home/user/share:

    sudo mount share

    
## Descargar Repositorio desde casa
para descargar nuestro repositorio desde casa realizaremos los siguientes pasos:

- Abrimos Jupyter Notebook [localhost:8008/tree](http://localhost:8008/tree) una vez que la máquina arranca.
- Abrimos un terminal  (damos click sobre new / terminal).
- En la consola escribimos cd CarpetaCompartida (por defecto, cd share)
- finalmente ejecutamos el comando: git clone https://gitlab.com/bivl2ab/pvd-courses-2019/semillero-2019-1/semillero-2019-1.git

El repositorio quedará descargado en /Share






